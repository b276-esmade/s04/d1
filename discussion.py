# Python OOP


class Sample_Class():
	def __init__(self, year):
		self.year = year


	def show_year(self):
		print(f"The year is: {self.year}")




sample_obj = Sample_Class(2023)

print(sample_obj.year)
sample_obj.show_year()




# [SECTION 0 ] Fundamentals of OOP

# 4 Pillars of OOP
	# Encapsulation
	# Inheritance
	# Abstraction
	# Polymorphism


# [SECTION 1 ] Encapsulation
	# mechanism of wrapping attributes and code acting on methods together as single unit
	# In encapsulation, the attributes of a class will be hidden from other classes and can be accessed only through the methods of their current class. Therefore, it is also known as data hiding.


class Person():
	def __init__(self,name,age):
		# "_" signifies private data
		self._name = name
		self._age = age


	# Getter/Setter Methods
	# getter of _name attribute
	def get_name(self):
		print(f"Name of person: {self._name}")

	# setter
	def set_name(self, name):
		self._name = name

	# getter of age
	def get_age(self):
		print(f"Age of person: {self._age}")


	# setter
	def set_age(self, age):
		self._age = age





# new instance
# NAME
person_one = Person("James", 245)

person_one.get_name() #James

person_one.set_name("Kat")
person_one.get_name() #Kat


# AGE
person_one.get_age() #245

person_one.set_age(3)
person_one.get_age() #3







# [SECTION 2 ]  Inheritance
	# Inheritance of the characteristic or attributes of a parent class to a child class that are derived from it

	# Syntax: class child_class_name(parent_class_name)

class Employee(Person):
	def __init__(self, name, age, employee_id):
		# super is to invoke immediate parent class constructor (Person)
		super().__init__(name, age) #to call attributes from Person

		self._employee_id = employee_id

	# methods
	def get_employee_id(self):
		print(f"The employee ID is {self._employee_id}")

	def set_employee_id(self, employee_id):
		self._employee_id = employee_id


# instantiation
employee_one = Employee("CongTV",27,"00001")

employee_one.get_age()
employee_one.set_age(28)
employee_one.get_age()


employee_one.get_name()
employee_one.set_name("Lincoln")
employee_one.get_name()


employee_one.get_employee_id()
employee_one.set_employee_id("00005")
employee_one.get_employee_id()



# [SECTION 3]  Polymorphism
	# many + forms

# Functions and objects

class Team_Lead():
	def occupation(self):
		print("Team Lead")

	def has_auth(self):
		print(True)

class Team_Member():
	def occupation(self):
		print("Team Member")

	def has_auth(self):
		print(False)




# instantiation
team_lead1 = Team_Lead()
team_member1 = Team_Member()

for person in (team_lead1,team_member1):
	person.occupation()
	person.has_auth()


# Polymorphism with Inheritance
	# decoupling(independent from each other) and coupling(dependent to each other)
class Zuitt():
	def tracks(self):
		print("We are currently ofering 3 tracks(developer career, pi-shape career and short courses)")


	def num_of_hours(self):
		print("Learn web development in 360 hours!")

# inheritance
class Developer_Career(Zuitt):
	def num_of_hours(self):
		print("Learn the basics of web dev in 240 hours") #change value


class Pi_Shape_Career(Zuitt):
	def num_of_hours(self):
		print("Learn skills for no-code app dev in 140 hours")

class Short_Courses(Zuitt):
	def num_of_hours(self):
		print("Learn advanced topics in web dev in 20 hours")


# instantiate
course1 = Developer_Career()
course2 = Pi_Shape_Career()
course3 = Short_Courses()


for course in (course1,course2,course3):
	course.tracks()
	course.num_of_hours()




# [SECTION 4]  Abstraction
	# An abstract class can be considered as blueprint for other classes. It allows you to create a set of methods that must be created within any child classes built from abstract class




# ABC - abstract base classes
# Walang gagawin
from abc import ABC
	# This import tells the program to get the abc module of python to be used

class Polygon(ABC):
	def print_no_of_sides(self):
		# The pass keyword it denotes that the method doesnt do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()
	def print_no_of_sides(self):
		print(f"This Polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()
	def print_no_of_sides(self):
		print(f"This Polygon has 5 sides")


# instantiate
shape1 = Triangle()
shape2 = Pentagon()

shape1.print_no_of_sides()
shape2.print_no_of_sides()